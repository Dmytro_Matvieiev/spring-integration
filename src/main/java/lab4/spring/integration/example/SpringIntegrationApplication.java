package lab4.spring.integration.example;

import lab4.spring.integration.example.domain.Order;
import lab4.spring.integration.example.service.ParceCsvService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import java.util.List;

@Slf4j
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SpringIntegrationApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SpringIntegrationApplication.class, args);
        DirectChannel outputChannel = context.getBean("outputChannel", DirectChannel.class);
        outputChannel.subscribe(message -> log.info("Order from output channel: {}", message.getPayload()));

        List<Order> orderList = ParceCsvService.removeCanceledOrder(ParceCsvService.getAllOrders());

        context.getBean(OrderService.class).process(orderList);
        context.close();

        orderList.stream().forEach(o -> log.info("Order number: {} with state : {}", o.getId(), o.getOrderState()));
    }

    @Bean
    DirectChannel outputChannel() {
        return new DirectChannel();
    }

    @MessagingGateway
    public interface OrderService {
        @Gateway(requestChannel = "orderFlow.input")
        void process(List<Order> orders);
    }

    @Bean
    public IntegrationFlow orderFlow() {
        return flow -> flow
                .handle("parceCsvService", "getAllOrders")
                .handle("parceCsvService", "removeCanceledOrder")
                .channel(outputChannel());
    }
}
