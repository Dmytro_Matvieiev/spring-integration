package lab4.spring.integration.example.domain;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Order {
    @CsvBindByName(column = "id")
    private long id;
    @CsvBindByName(column = "orderState")
    private OrderState orderState;
}
