package lab4.spring.integration.example.service;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lab4.spring.integration.example.domain.Order;
import lab4.spring.integration.example.domain.OrderState;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class ParceCsvService {

    private static final String csvFilename = "src/order.csv";

    public static List<Order> getAllOrders() {

        log.info("Start getting all orders at - {}", LocalDateTime.now());

        try (Reader reader = Files.newBufferedReader(Paths.get(csvFilename))) {
            CsvToBean<Order> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(Order.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            return csvToBean.parse();

        } catch (IOException ex) {
            log.error("Error getAllOrders : {}, {}",
                    ExceptionUtils.getMessage(ex), ExceptionUtils.getMessage(ex.getCause()));
        }
        return Collections.emptyList();
    }

    public static List<Order> removeCanceledOrder(List<Order> list) {
        list.removeIf(o -> o.getOrderState().equals(OrderState.CANCELED));
        return list;
    }
}
