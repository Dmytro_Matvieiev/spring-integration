package lab4.spring.integration.example;

import lab4.spring.integration.example.domain.Order;
import lab4.spring.integration.example.service.ParceCsvService;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ParseCsvServiceTest {

    @Test
    void shouldParseCsvAndReturnOrders() {
        List<Order> orders = ParceCsvService.getAllOrders();
        Integer size = orders.size();
        assertNotNull(size);
        assertEquals(5, size);
    }

    @Test
    void shouldRemoveOrderWithCanceledStatus() {
        List<Order> ordersWithFilter = ParceCsvService.removeCanceledOrder(ParceCsvService.getAllOrders());
        List<Order> ordersWithoutFilter = ParceCsvService.getAllOrders();
        assertNotEquals(ordersWithFilter.size(), ordersWithoutFilter.size());
        assertNotEquals(ordersWithFilter, ordersWithoutFilter);
    }
}
